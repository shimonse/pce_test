import asyncio
import itertools
import logging
from collections import namedtuple
from functools import partial
from os import path
from typing import FrozenSet, Dict, Any, Optional, Tuple, Union, Iterable, List, Set
from enum import Enum

from nfdk.model_v2.simple_model_pojo_pool import SimpleModelPojoPool
from nfdk.pojos.model import Port, EthPort, EthLink, DeviceInventoryItem, Path, PathHop, ServiceIntent, ServiceIntentResource, Service, \
    OtnLineServiceIntent, ELineServiceIntent, ETreeServiceIntent, ELanServiceIntent, L3VpnServiceIntent, SdhLineServiceIntent, ELineServiceIntentRawEthServiceData, \
    PathComputationSettings, ProtectionSettings, ResourceDiversity, ELineServiceIntentUnderlayTech, OtnLineServiceIntentUnderlayTech, MplsUnderlayTech, UnderlayTechIpMpls, UnderlayTechOtn, UnderlayTechWdm, UnderlayTechMplsTp, \
    QosSettings, L3VpnAnyToAny, L3VpnHubAndSpoke, EndpointServiceIntentResource, IncludeServiceIntentResource, ExcludeServiceIntentResource, NniServiceIntentResource, \
    SIEndpointLayer1Info, SIEndpointLayer2Info, SIEndpointLayer3Info, CePeSettings, CePeSettingsOspfRouting, CePeSettingsBgpRouting, CePeSettingsStaticRouting, CePeSettingsStaticRoutingEntry, DeploymentInfo
from nfdk.pojos.model_enums import PortRelDirectionEnum, PathHopDirectionEnum, PortOperStatusEnum, PortAdminStatusEnum, \
    EthPortTypeEnum, UnderlayTechOtnServiceTunnelRateEnum, DiversityPolicyEnum, DiversedResourcesEnum, \
    LinkStateEnum, ProtectionPolicyEnum, UnderlayTechIpMplsTunnelTypeEnum, ELineServiceIntentSupportedUnderlayTechsEnum, \
    UnderlayTechMplsTpTunnelUsageConstraintsEnum, UnderlayTechNNITechEnum, UnderlayTechOtnServiceTunnelTypeEnum, ServiceIntentTypeEnum, \
    UnderlayTechMplsTunnelResilliencyEnum, PortTypeEnum, LinkLayerEnum, LinkPathGroupTypeEnum, LinkRoleEnum, \
    LinkProtectionStatusEnum, PortCapabilitiesEnum, ServiceIntentResourceTypeEnum, L3VpnRoleEnum, CePeSettingsRoutingMethodEnum, ProtectionRoleEnum, VlanManipulationEnum, \
    DeploymentOperationEnum, DeploymentOperationPhaseEnum, DeploymentTypeEnum
from nfdk.pyoneer.app import App
from nfdk.pyoneer.apps import ReportAppUI
from nfdk.pyoneer.components import ActionSheet, Action, EmbeddedAppTemplate, JsxComponent, TextLabel, DialogBox, \
    Container, Cardbox, Table
from nfdk.pyoneer.web_server import WebServer
from path_calculation_engine_pkg.datatypes import Service as PceService, ServiceType, Settings, Endpoint, PathOptimizationEnum, \
    PathSettings, PathComputationSettings as PcePathComputationSettings, GeneralSettings, TunnelPolicy
from path_calculation_engine_pkg.external import build_service_request, calculate_service_path
from path_calculation_engine_pkg.model_updater import ModelHelper
from path_calculation_engine_pkg.pce_errors import CustomError
from path_calculation_engine_pkg.pce_utils import CalculatedPaths, CalculatedPath
from path_calculation_engine_pkg.pce_utils import ReportUtils, ServiceRequestConstants

ELINE_OTN_BASE={
    ServiceRequestConstants.SERVICE_TYPE: ServiceIntentTypeEnum.E_LINE,
    ELineServiceIntentSupportedUnderlayTechsEnum.OTN: UnderlayTechOtn.create_new(
        service_tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.ODU3E2,
        service_tunnel_type=UnderlayTechOtnServiceTunnelTypeEnum.ODU_K,
        allowed_nni=frozenset(
            {
                UnderlayTechNNITechEnum.OTU4,
                UnderlayTechNNITechEnum.OTU3,
                UnderlayTechNNITechEnum.OTU3E2,
                UnderlayTechNNITechEnum.OTU2,
                UnderlayTechNNITechEnum.OTU2E
            }
        )
    ),
    ServiceRequestConstants.SERVICE_NAME:"Base Unprotected ELINE OTN ODU2E",
    ServiceRequestConstants.PRIMARY_PATH: {
        ServiceRequestConstants.PATH_OPTIMIZATION_TYPE: PathOptimizationEnum.NUMBER_OF_HOPS.name,
        ServiceRequestConstants.INCLUDE_RESOURCES: [
            "LI/odu/8c30163e46fc97ae/df9e4b6ab4566374/5979a210307b1e66/fd70f7cff7be2cdc"
        ],
        ServiceRequestConstants.EXCLUDE_RESOURCES: [

        ]
    },
    ServiceRequestConstants.SECONDARY_PATH: {
        ServiceRequestConstants.PATH_OPTIMIZATION_TYPE: PathOptimizationEnum.NUMBER_OF_HOPS.name,
        ServiceRequestConstants.INCLUDE_RESOURCES: [

        ],
        ServiceRequestConstants.EXCLUDE_RESOURCES: [

        ]
    },
    ServiceRequestConstants.PROTECTION_POLICY:ProtectionPolicyEnum.UNPROTECTED.name,
    ServiceRequestConstants.DIVERSITY_RESOURCES: {},
    ServiceRequestConstants.ALLOWED_UNDERLAY_OPTIONS: ELineServiceIntentSupportedUnderlayTechsEnum.OTN,
    ServiceRequestConstants.ENDPOINTS:[
        {
            ServiceRequestConstants.GUID:"PO/optical_client/44c2c933297e5bda/7d8b2f5d3dd9fe15",
            "l1":SIEndpointLayer1Info.create_new(
                tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.ODU3E2
            )
        },
        {
            ServiceRequestConstants.GUID:"PO/optical_client/d9367cab165bb239/7d8b2f5d3dd9fe15",
            "l1":SIEndpointLayer1Info.create_new(
                tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.ODU3E2
            )
        }
    ]
}

ELINE_OTN_BASE_1_plus_1_PROTECTED = {
    ServiceRequestConstants.SERVICE_TYPE: ServiceIntentTypeEnum.E_LINE,
    ELineServiceIntentSupportedUnderlayTechsEnum.OTN: UnderlayTechOtn.create_new(
        service_tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.ODU2E,
        service_tunnel_type=UnderlayTechOtnServiceTunnelTypeEnum.ODU_K,
        allowed_nni=frozenset(
            {
                UnderlayTechNNITechEnum.OTU4,
                UnderlayTechNNITechEnum.OTU3,
                UnderlayTechNNITechEnum.OTU3E2,
                UnderlayTechNNITechEnum.OTU2,
                UnderlayTechNNITechEnum.OTU2E
            }
        )
    ),
    ServiceRequestConstants.SERVICE_NAME:"Base 1 plus 1 protected ELINE OTN ODU2E",
    ServiceRequestConstants.PRIMARY_PATH: {
        ServiceRequestConstants.PATH_OPTIMIZATION_TYPE: PathOptimizationEnum.LATENCY.name,
        ServiceRequestConstants.INCLUDE_RESOURCES:[

        ],
        ServiceRequestConstants.EXCLUDE_RESOURCES: [

        ]
    },
    ServiceRequestConstants.SECONDARY_PATH: {
        ServiceRequestConstants.PATH_OPTIMIZATION_TYPE: PathOptimizationEnum.LATENCY.name,
        ServiceRequestConstants.INCLUDE_RESOURCES: [

        ],
        ServiceRequestConstants.EXCLUDE_RESOURCES: [

        ]
    },
    ServiceRequestConstants.PROTECTION_POLICY:ProtectionPolicyEnum.INTRA_AREA_1_PLUS_1_PROTECTED.name,
    ServiceRequestConstants.DIVERSITY_RESOURCES: {},
    ServiceRequestConstants.ALLOWED_UNDERLAY_OPTIONS: ELineServiceIntentSupportedUnderlayTechsEnum.OTN,
    ServiceRequestConstants.ENDPOINTS: [
        {
            ServiceRequestConstants.GUID: "PO/optical_client/44c2c933297e5bda/7d8b2f5d3dd9fe15",
            "l1": SIEndpointLayer1Info.create_new(
                tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.ODU2E
            )
        },
        {
            ServiceRequestConstants.GUID: "PO/optical_client/d9367cab165bb239/7d8b2f5d3dd9fe15",
            "l1": SIEndpointLayer1Info.create_new(
                tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.ODU2E
            )
        }
    ]
}


LOG=logging.getLogger("PCE TESTER")


class MessageBox(JsxComponent):
    def __init__(self, title='', closable=True, visible=False):
        super().__init__()
        self.title = title
        self.msg = TextLabel()
        self.dialog = DialogBox(title=title, visible=visible, closable=closable, children=[self.msg],
                                on_close=self._on_close_cb)

    def declare(self, app):
        return self.dialog.declare(app)

    def _on_close_cb(self):
        self.dialog.visible = False

    def set_message(self, msg):
        self.dialog.visible = True
        self.msg.text = msg


HEADERS = {
    ServiceRequestConstants.HOP: "Hop",
    f"{ServiceRequestConstants.LINK}": "Link",
    f"{ServiceRequestConstants.SOURCE}_{ServiceRequestConstants.DEVICE}": "Src. Device",
    f"{ServiceRequestConstants.SOURCE}_{ServiceRequestConstants.PORT}": "Src. Port",
    f"{ServiceRequestConstants.SOURCE}_{ServiceRequestConstants.DOMAIN}": "Src. Domain",
    f"{ServiceRequestConstants.SOURCE}_{ServiceRequestConstants.TPN}": "Src. TPN",
    f"{ServiceRequestConstants.DESTINATION}_{ServiceRequestConstants.DEVICE}": "Dst. Device",
    f"{ServiceRequestConstants.DESTINATION}_{ServiceRequestConstants.PORT}": "Dst. Port",
    f"{ServiceRequestConstants.DESTINATION}_{ServiceRequestConstants.DOMAIN}": "Dst. Domain",
    f"{ServiceRequestConstants.DESTINATION}_{ServiceRequestConstants.TPN}": "Dst. TPN",
    ServiceRequestConstants.PORT_TYPE: "Type",
    ServiceRequestConstants.USED_CAPACITY: "Used",
    ServiceRequestConstants.AVAILABLE_CAPACITY: "Available",
    ServiceRequestConstants.LATENCY: "Latency",
    ServiceRequestConstants.ADMIN_COST: "Adm. Cost",
    ServiceRequestConstants.AGGREGATED_WEIGHT: "Weight"
}

PathHopRow = namedtuple('PathHopRow', [x.lower().replace(' ', '_') for x in HEADERS.keys()])

def get_prev_config()->PceService:
    return PceService(
            endpoints=[
                Endpoint(
                    port='PO/optical_client/HUAWEI-ACTN/0.190.0.3/16777228',
                    vlan_id=1000,
                    cir=1000,
                    capability=None,
                    eir=None,
                    cbs=None,
                    ebs=None
                ),
                Endpoint(
                    port='PO/optical_client/HUAWEI-ACTN/0.191.0.3/16777228',
                    vlan_id=1000,
                    cir=1000,
                    capability=None,
                    eir=None,
                    cbs=None,
                    ebs=None
                ),
            ],
            service_name="Test 2.3.1 without constraints",
            service_type=ServiceType.E_LINE,
            settings=Settings(
                general_settings=GeneralSettings(
                    allowed_underlay_options=ELineServiceIntentSupportedUnderlayTechsEnum.MPLS_TP,
                    service_tunnel_type=UnderlayTechOtnServiceTunnelTypeEnum.ODU_FLEX,
                    service_tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.AUTO_SIZE,
                    protection_policy=ProtectionPolicyEnum.UNPROTECTED,
                    allowed_nni_options=[UnderlayTechNNITechEnum.OTU2, UnderlayTechNNITechEnum.ETH_40G,
                                         UnderlayTechNNITechEnum.OTU3, UnderlayTechNNITechEnum.OTU4,
                                         UnderlayTechNNITechEnum.ETH_100G],
                    tunnel_policy=TunnelPolicy(
                        technology_type=None,
                        tunnel_usage_constraints=UnderlayTechMplsTpTunnelUsageConstraintsEnum.CREATE_IF_NOT_EXIST,
                        tunnel_resiliency=UnderlayTechMplsTunnelResilliencyEnum.NO_RESILLIENCY
                    )
                ),
                path_computation_settings=PcePathComputationSettings(
                    included_link_states=[
                        LinkStateEnum.OPERATIONALLY_UP
                    ],
                    diversity_resources=[
                        DiversedResourcesEnum.LINK,
                        DiversedResourcesEnum.SRLG
                    ],
                    diversity_policy=DiversityPolicyEnum.STRICT
                ),
                primary_path_settings=PathSettings(
                    path_optimization_type=PathOptimizationEnum.NUMBER_OF_HOPS,
                    include_resources=[
                    ],
                    exclude_resources=[
                    ]
                ),
                # secondary_path_settings=PathSettings(
                #     path_optimization_type=PathOptimizationEnum.NUMBER_OF_HOPS,
                #     include_resources=[
                #
                #     ],
                #     exclude_resources=[
                #     ]
                # )
            )
        )


class PathCalculatorMock(ReportAppUI):

    def __init__(self, time_stamp: int = -1):
        super().__init__(
            title="Path Calculator Mock",
            description="Mock implementation of Path Calculator for Service"
        )

        self._msg_box = MessageBox(title='Path Calculator Algorithm')
        self.action = Action(title='Execute', style=Action.Style.Outlined, callback=self.execute_in_background)
        self.input_params_text = TextLabel(text='')
        self.input_params = Cardbox(title='Summary input params', children=[self.input_params_text])
        self.summary_metadata_text = TextLabel(text='')
        self.calculated_paths_metadata = Cardbox(title='Algorithm summary', children=[self.summary_metadata_text])
        self.primary_path_summary = TextLabel(text='Primary Path Summary')
        self.primary_path_result = Table(headers=[x for x in HEADERS.values()], rows=[])
        self.secondary_path_summary = TextLabel(text='Secondary Path Summary')
        self.secondary_path_result = Table(headers=[x for x in HEADERS.values()], rows=[])

        self.results = Container(
            # css_class='path-view-container',
            children=[
                self.input_params,
                self.calculated_paths_metadata,
                Cardbox(title='Primary Path', children=[
                    self.primary_path_summary,
                    self.primary_path_result
                ]),
                Cardbox(title='Secondary Path', children=[
                    self.secondary_path_summary,
                    self.secondary_path_result
                ])
            ])
        self.template = EmbeddedAppTemplate(title='Path Calculator Emulation',
                                            children=[
                                                self._msg_box,
                                                ActionSheet(actions=[self.action]),
                                                self.results
                                            ])
        self.set_root(root_view=self.template)
        self._model_helper = ModelHelper()

    def execute_in_background(self):
        asyncio.ensure_future(self.execute())

    async def visualize_proposed_paths_si(self, si: ServiceIntent, model: SimpleModelPojoPool,
                                       calculated_paths: CalculatedPaths):
        self.summary_metadata_text.text = str(calculated_paths.metadata)
        self.input_params_text.text = si.to_json(include_backrefs=False)
        primary_path = calculated_paths.primary_path
        hope_num_primary = 0
        prim_rows = []
        if primary_path and isinstance(primary_path, CalculatedPath):
            self.primary_path_summary.text += "\n" + str(primary_path.path_metadata)
            if primary_path.path_entries:
                for path_entry in primary_path.path_entries:
                    hope_num_primary += 1
                    src = model.get_by_guid(path_entry.src.port)  # type: Port
                    dst = model.get_by_guid(path_entry.dst.port)  # type: Port
                    prim_rows.append(
                        Table.Rows.Expandable(
                            cols=PathHopRow(
                                hop=hope_num_primary,
                                link=path_entry.attributes[ServiceRequestConstants.LINK],
                                source_device=ReportUtils.build_one_href(one=src.device_lazy_err),
                                source_port=ReportUtils.build_optical_port_href(port=src),
                                source_domain=path_entry.src.domain,
                                source_tpn=path_entry.src.tpn,
                                destination_device=ReportUtils.build_one_href(one=dst.device_lazy_err),
                                destination_port=ReportUtils.build_optical_port_href(port=dst),
                                destination_domain=path_entry.dst.domain,
                                destination_tpn=path_entry.dst.tpn,
                                port_type=path_entry.type,
                                used_capacity=path_entry.used_capability,
                                available_capacity=path_entry.available_capability,
                                latency=path_entry.attributes[ServiceRequestConstants.LATENCY],
                                admin_cost=path_entry.attributes[ServiceRequestConstants.ADMIN_COST],
                                aggregated_weight=path_entry.attributes[ServiceRequestConstants.AGGREGATED_WEIGHT]
                            ),
                            children=[]
                        )
                    )
                self.primary_path_result.rows = prim_rows

        hope_num_secondary = 0
        sec_rows = []
        secondary_path = calculated_paths.secondary_path
        if secondary_path and isinstance(secondary_path, CalculatedPath):
            self.secondary_path_summary.text += "\n" + str(secondary_path.path_metadata)
            if secondary_path.path_entries:
                for path_entry in secondary_path.path_entries:
                    hope_num_secondary += 1
                    src = model.get_by_guid(path_entry.src.port)  # type: Port
                    dst = model.get_by_guid(path_entry.dst.port)  # type: Port

                    sec_rows.append(
                        Table.Rows.Expandable(
                            cols=PathHopRow(
                                hop=hope_num_secondary,
                                link=path_entry.attributes[ServiceRequestConstants.LINK],
                                source_device=ReportUtils.build_one_href(one=src.device_lazy_err),
                                source_port=ReportUtils.build_optical_port_href(port=src),
                                source_domain=path_entry.src.domain,
                                source_tpn=path_entry.src.tpn,
                                destination_device=ReportUtils.build_one_href(one=dst.device_lazy_err),
                                destination_port=ReportUtils.build_optical_port_href(port=dst),
                                destination_domain=path_entry.dst.domain,
                                destination_tpn=path_entry.dst.tpn,
                                port_type=path_entry.type,
                                used_capacity=path_entry.used_capability,
                                available_capacity=path_entry.available_capability,
                                latency=path_entry.attributes[ServiceRequestConstants.LATENCY],
                                admin_cost=path_entry.attributes[ServiceRequestConstants.ADMIN_COST],
                                aggregated_weight=path_entry.attributes[ServiceRequestConstants.AGGREGATED_WEIGHT]
                            ),
                            children=[]
                        )
                    )
                self.secondary_path_result.rows = sec_rows

    async def visualize_proposed_paths(self, config: PceService, model: SimpleModelPojoPool,
                                       calculated_paths: CalculatedPaths):
        self.summary_metadata_text.text = str(calculated_paths.metadata)
        self.input_params_text.text = str(config)
        primary_path = calculated_paths.primary_path
        hope_num_primary = 0
        prim_rows = []
        if primary_path and isinstance(primary_path, CalculatedPath):
            self.primary_path_summary.text += "\n" + str(primary_path.path_metadata)
            if primary_path.path_entries:
                for path_entry in primary_path.path_entries:
                    hope_num_primary += 1
                    src = model.get_by_guid(path_entry.src.port)  # type: Port
                    dst = model.get_by_guid(path_entry.dst.port)  # type: Port
                    prim_rows.append(
                        Table.Rows.Expandable(
                            cols=PathHopRow(
                                hop=hope_num_primary,
                                link=path_entry.attributes[ServiceRequestConstants.LINK],
                                source_device=ReportUtils.build_one_href(one=src.device_lazy_err),
                                source_port=ReportUtils.build_optical_port_href(port=src),
                                source_domain=path_entry.src.domain,
                                source_tpn=path_entry.src.tpn,
                                destination_device=ReportUtils.build_one_href(one=dst.device_lazy_err),
                                destination_port=ReportUtils.build_optical_port_href(port=dst),
                                destination_domain=path_entry.dst.domain,
                                destination_tpn=path_entry.dst.tpn,
                                port_type=path_entry.type,
                                used_capacity=path_entry.used_capability,
                                available_capacity=path_entry.available_capability,
                                latency=path_entry.attributes[ServiceRequestConstants.LATENCY],
                                admin_cost=path_entry.attributes[ServiceRequestConstants.ADMIN_COST],
                                aggregated_weight=path_entry.attributes[ServiceRequestConstants.AGGREGATED_WEIGHT]
                            ),
                            children=[]
                        )
                    )
                self.primary_path_result.rows = prim_rows

        hope_num_secondary = 0
        sec_rows = []
        secondary_path = calculated_paths.secondary_path
        if secondary_path and isinstance(secondary_path, CalculatedPath):
            self.secondary_path_summary.text += "\n" + str(secondary_path.path_metadata)
            if secondary_path.path_entries:
                for path_entry in secondary_path.path_entries:
                    hope_num_secondary += 1
                    src = model.get_by_guid(path_entry.src.port)  # type: Port
                    dst = model.get_by_guid(path_entry.dst.port)  # type: Port

                    sec_rows.append(
                        Table.Rows.Expandable(
                            cols=PathHopRow(
                                hop=hope_num_secondary,
                                link=path_entry.attributes[ServiceRequestConstants.LINK],
                                source_device=ReportUtils.build_one_href(one=src.device_lazy_err),
                                source_port=ReportUtils.build_optical_port_href(port=src),
                                source_domain=path_entry.src.domain,
                                source_tpn=path_entry.src.tpn,
                                destination_device=ReportUtils.build_one_href(one=dst.device_lazy_err),
                                destination_port=ReportUtils.build_optical_port_href(port=dst),
                                destination_domain=path_entry.dst.domain,
                                destination_tpn=path_entry.dst.tpn,
                                port_type=path_entry.type,
                                used_capacity=path_entry.used_capability,
                                available_capacity=path_entry.available_capability,
                                latency=path_entry.attributes[ServiceRequestConstants.LATENCY],
                                admin_cost=path_entry.attributes[ServiceRequestConstants.ADMIN_COST],
                                aggregated_weight=path_entry.attributes[ServiceRequestConstants.AGGREGATED_WEIGHT]
                            ),
                            children=[]
                        )
                    )
                self.secondary_path_result.rows = sec_rows

    async def _modify(self, si: bool):
        await self._model_helper.run(self.app)
        model = self._model_helper.pool

        # self._new_config, reconstructed_paths = await reconstruct_service_path(app=self.app, service_guid="LI/persist/stitched-eth-LI/eth/HUAWEI-ACTN/0.190.0.3/50266113:halflink~LI/eth/HUAWEI-ACTN/0.191.0.4/50266113:halflink")
        if si:
            guid_service_to_modify = "SI/AcmeMAD-BCN"
            prev_si: ServiceIntent = model.get_by_guid(guid_service_to_modify)
            new_si: ServiceIntent = self._copy_si(orig=prev_si)
            new_si.set_related_services(value=None)
            resources: Iterable[ServiceIntentResource] = new_si.resources
            endpoints: Set[ServiceIntentResource] = set()
            main_includes: Set[ServiceIntentResource] = set()
            main_excludes: Set[ServiceIntentResource] = set()
            main_nnis: Set[ServiceIntentResource] = set()
            backup_excludes: Set[ServiceIntentResource] = set()
            backup_includes: Set[ServiceIntentResource] = set()
            backup_nnis: Set[ServiceIntentResource] = set()
            new_resources: Set[ServiceIntentResource] = set()
            for resource in resources:
                if resource.type == ServiceIntentResourceTypeEnum.ENDPOINT:
                    resource: EndpointServiceIntentResource = resource
                    l1: SIEndpointLayer1Info = resource.layer_1_info_non_throwing
                    new_l1: SIEndpointLayer1Info = None
                    if l1:
                        tunnel_rate: UnderlayTechOtnServiceTunnelRateEnum = l1.tunnel_rate
                        new_l1 = SIEndpointLayer1Info.create_new(
                            tunnel_rate=tunnel_rate
                        )
                    l2: SIEndpointLayer2Info = resource.layer_2_info_non_throwing
                    new_l2: SIEndpointLayer2Info = None
                    if l2:
                        se_tags: Optional[FrozenSet[int]] = l2.mapped_ce_tags
                        new_se_tags = se_tags if se_tags else None
                        qos: Optional[QosSettings] = l2.qos_non_throwing
                        new_qos = QosSettings.create_new(
                            cir=10000,
                            eir = qos.eir if qos else None,
                            ebs_kb = qos.ebs_kb if qos else None,
                            cbs_kb = qos.cbs_kb if qos else None,
                            qos_policy = qos.qos_policy if qos else None
                        )
                        new_l2= SIEndpointLayer2Info.create_new(
                            mapped_ce_tags=new_se_tags,
                            qos=new_qos
                        )
                    l3: SIEndpointLayer3Info = resource.layer_3_info_non_throwing
                    new_l3: SIEndpointLayer3Info = None
                    if l3:
                        ce_pe_settings: CePeSettings = l3.ce_pe_settings_non_throwing
                        new_ce_pe_settings: CePeSettings = None
                        if ce_pe_settings:
                            ospf: CePeSettingsOspfRouting = ce_pe_settings.ospf_routing_details_non_throwing
                            new_ospf: CePeSettingsOspfRouting = None
                            if ospf:
                                new_ospf = CePeSettingsOspfRouting.create_new(
                                    metric=ospf.metric,
                                    ospf_area=ospf.ospf_area
                                )
                            bgp: CePeSettingsBgpRouting = ce_pe_settings.bgp_routing_details_non_throwing
                            new_bgp: CePeSettingsBgpRouting = None
                            if bgp:
                                new_bgp = CePeSettingsBgpRouting.create_new(
                                    autonomous_system=bgp.autonomous_system,
                                    peering_ip=bgp.peering_ip
                                )
                            static: CePeSettingsStaticRouting = ce_pe_settings.static_routing_details_non_throwing
                            new_static: CePeSettingsStaticRouting = None
                            if static and static.entries:
                                new_static = CePeSettingsStaticRouting.create_new(
                                    entries=tuple(static.entries)
                                )
                            routing_method: CePeSettingsRoutingMethodEnum = ce_pe_settings.routing_method
                            new_ce_pe_settings = CePeSettings.create_new(
                                routing_method=routing_method,
                                ospf_routing_details=new_ospf,
                                bgp_routing_details=new_bgp,
                                static_routing_details=new_static
                            )
                        new_l3 = SIEndpointLayer3Info.create_new(
                            ip_address=l3.ip_address,
                            l_3_vpn_role=l3.l_3_vpn_role,
                            ce_pe_settings=new_ce_pe_settings
                        )
                    new_endpoint_resource: EndpointServiceIntentResource = EndpointServiceIntentResource.create_new(
                        guid=resource.guid,
                        layer_1_info=new_l1,
                        layer_2_info=new_l2,
                        layer_3_info=new_l3,
                        resource=model.get_by_guid(guid=resource.guid),
                        order=resource.order,
                        service_intent=new_si,
                        extra=resource.extra
                    )
                    endpoints.add(new_endpoint_resource)

                elif resource.type == ServiceIntentResourceTypeEnum.EXCLUDE:
                    resource: ExcludeServiceIntentResource = resource
                    protection_role: ProtectionRoleEnum = resource.protection_role
                    new_exclude_resource = ExcludeServiceIntentResource.create_new(
                        guid=resource.guid,
                        order=resource.order,
                        resource=model.get_by_guid(guid=resource.guid),
                        protection_role=protection_role,
                        service_intent=new_si,
                        extra=resource.extra
                    )
                    if protection_role == ProtectionRoleEnum.PROTECTION_PRIMARY:
                        main_excludes.add(new_exclude_resource)
                    elif protection_role == ProtectionRoleEnum.PROTECTION_SECONDARY:
                        backup_excludes.add(new_exclude_resource)
                    elif protection_role == ProtectionRoleEnum.PROTECTION_ALL:
                        main_excludes.add(new_exclude_resource)
                        backup_excludes.add(new_exclude_resource)
                elif resource.type == ServiceIntentResourceTypeEnum.INCLUDE:
                    resource: IncludeServiceIntentResource = resource
                    protection_role: ProtectionRoleEnum = resource.protection_role
                    new_include_resource = IncludeServiceIntentResource.create_new(
                        guid=resource.guid,
                        order=resource.order,
                        resource=model.get_by_guid(guid=resource.guid),
                        protection_role=protection_role,
                        service_intent=new_si,
                        extra=resource.extra
                    )
                    if protection_role == ProtectionRoleEnum.PROTECTION_PRIMARY:
                        main_includes.add(new_include_resource)
                    elif protection_role == ProtectionRoleEnum.PROTECTION_SECONDARY:
                        backup_includes.add(new_include_resource)
                    elif protection_role == ProtectionRoleEnum.PROTECTION_ALL:
                        main_includes.add(new_include_resource)
                        backup_includes.add(new_include_resource)
                elif resource.type == ServiceIntentResourceTypeEnum.NNI:
                    resource: NniServiceIntentResource = resource
                    protection_role: ProtectionRoleEnum = resource.protection_role
                    new_nni_resource = NniServiceIntentResource.create_new(
                        guid=resource.guid,
                        order=resource.order,
                        resource=model.get_by_guid(guid=resource.guid),
                        protection_role=protection_role,
                        service_intent=new_si,
                        extra=resource.extra
                    )
                    if protection_role == ProtectionRoleEnum.PROTECTION_PRIMARY:
                        main_nnis.add(new_nni_resource)
                    elif protection_role == ProtectionRoleEnum.PROTECTION_SECONDARY:
                        backup_nnis.add(new_nni_resource)
                    elif protection_role == ProtectionRoleEnum.PROTECTION_ALL:
                        main_nnis.add(new_nni_resource)
                        backup_nnis.add(new_nni_resource)
            new_resources.update(endpoints)
            new_resources.update(main_includes)
            new_resources.update(main_excludes)
            new_resources.update(main_nnis)
            new_resources.update(backup_includes)
            new_resources.update(backup_excludes)
            new_resources.update(backup_nnis)
            new_si.set_resources(value=frozenset(new_resources))
            pool, proposed_paths = await self._calculate_paths_si(curr=new_si, prev=prev_si)
        else:
            guid_service_to_modify = "SI/AcmeMAD-BCN"
            previous_config: PceService = get_prev_config()
            new_config: PceService = self._copy_service(orig=previous_config)
            new_config.endpoints[0].cir = 10000
            new_config.endpoints[1].cir = 10000
            pool, proposed_paths = await self._calculate_proposed_paths(new_config=new_config,
                                                                        previous_config=previous_config,
                                                                        guid_service_to_modify=guid_service_to_modify)
            # new_config.settings.general_settings.protection_policy = ProtectionPolicyEnum.UNPROTECTED
        # new_config.settings.primary_path_settings.exclude_resources = [
        #     'LI/persist/stitched-idl-PO/otu/HUAWEI-ACTN/0.190.0.1/121~PO/otu/HUAWEI-ACTN/0.191.0.1/175'
        # ]



        await self.visualize_proposed_paths(config=new_config, model=pool, calculated_paths=proposed_paths)

    def _copy_endpoint(self, orig: Endpoint) -> Endpoint:
        return Endpoint(
            port=orig.port,
            vlan_id=orig.vlan_id,
            cir=orig.cir,
            capability=orig.capability,
            eir=orig.eir,
            cbs=orig.cbs,
            ebs=orig.ebs
        )

    def _copy_general_settings(self, orig: GeneralSettings) -> GeneralSettings:
        return GeneralSettings(
            allowed_underlay_options=orig.allowed_underlay_options,
            service_tunnel_type=orig.service_tunnel_type,
            service_tunnel_rate=orig.service_tunnel_rate,
            protection_policy=orig.protection_policy,
            allowed_nni_options=[x for x in orig.allowed_nni_options],
            tunnel_policy=TunnelPolicy(
                technology_type=orig.tunnel_policy.technology_type,
                tunnel_usage_constraints=orig.tunnel_policy.tunnel_usage_constraints,
                tunnel_resiliency=orig.tunnel_policy.tunnel_resiliency
            )
        )

    def _copy_path_computation_setiings(self, orig: PcePathComputationSettings) -> PcePathComputationSettings:
        return PcePathComputationSettings(
            included_link_states=[x for x in orig.included_link_states],
            diversity_resources=[y for y in orig.diversity_resources],
            diversity_policy=orig.diversity_policy
        )

    def _copy_path_settings(self, orig: PathSettings):
        if orig:
            return PathSettings(
                path_optimization_type=orig.path_optimization_type,
                include_resources=[x for x in orig.include_resources],
                exclude_resources=[y for y in orig.exclude_resources]
            )

    def _copy_si(self, orig: ServiceIntent)->ServiceIntent:
        def copy_ip_mpls_tech(orig_ip_mpls: Optional[UnderlayTechIpMpls]) -> Optional[UnderlayTechIpMpls]:
            if not orig_ip_mpls:
                return None
            return UnderlayTechIpMpls.create_new(
                allowed_nni=frozenset(x for x in orig_ip_mpls.allowed_nni) if orig_ip_mpls.allowed_nni else frozenset(),
                tunnel_resilliency=orig_ip_mpls.tunnel_resilliency,
                tunnel_types=orig_ip_mpls.tunnel_types
            )

        def copy_l3vpn_hub_and_spoke(orig_details: Optional[L3VpnHubAndSpoke])->Optional[L3VpnHubAndSpoke]:
            if not orig_details:
                return None
            return L3VpnHubAndSpoke.create_new(
                minimum_hub_sites=orig_details.minimum_hub_sites,
                minimum_spoke_sites = orig_details.minimum_spoke_sites,
                hub_route_target = orig_details.hub_route_target,
                spoke_route_target=orig_details.spoke_route_target
            )

        def copy_l3vpn_any_to_any(orig_details: Optional[L3VpnAnyToAny])->Optional[L3VpnAnyToAny]:
            if not orig_details:
                return None
            return L3VpnAnyToAny.create_new(
                minimum_sites=orig_details.minimum_sites,
                route_target=orig_details.route_target
            )

        def copy_mpls_tp_tech(orig_mpls_tp: Optional[UnderlayTechMplsTp]) -> Optional[UnderlayTechMplsTp]:
            if not orig_mpls_tp:
                return None
            return UnderlayTechMplsTp.create_new(
                allowed_nni=frozenset(x for x in orig_mpls_tp.allowed_nni) if orig_mpls_tp.allowed_nni else frozenset(),
                tunnel_resilliency=orig_mpls_tp.tunnel_resilliency,
                tunnel_usage_constraints=orig_mpls_tp.tunnel_usage_constraints
            )

        def copy_otn_tech(orig_otn: Optional[UnderlayTechOtn]) -> Optional[UnderlayTechOtn]:
            if not orig_otn:
                return None
            return UnderlayTechOtn.create_new(
                allowed_nni=frozenset(x for x in orig_otn.allowed_nni) if orig_otn.allowed_nni else frozenset(),
                service_tunnel_rate=orig_otn.service_tunnel_rate,
                service_tunnel_type=orig_otn.service_tunnel_type
            )

        def copy_wdm_tech(orig_wdm: Optional[UnderlayTechWdm]) -> Optional[UnderlayTechWdm]:
            if not orig_wdm:
                return None
            return UnderlayTechWdm.create_new(
                allowed_nni=frozenset(x for x in orig_wdm.allowed_nni) if orig_wdm.allowed_nni else frozenset()
            )

        def copy_l3vpn_underlay_tech(orig_utech: Optional[UnderlayTechIpMpls])->Optional[UnderlayTechIpMpls]:
            if not orig_utech:
                return None
            return UnderlayTechIpMpls.create_new(
                allowed_nni=frozenset(x for x in orig_utech.allowed_nni) if orig_utech.allowed_nni else frozenset(),
                tunnel_resilliency=orig_utech.tunnel_resilliency,
                tunnel_types=tuple(x for x in orig_utech.tunnel_types) if orig_utech.tunnel_types else None
            )

        def copy_otn_line_underlay_tech(orig_utech: Optional[OtnLineServiceIntentUnderlayTech])->Optional[OtnLineServiceIntentUnderlayTech]:
            if not orig_utech:
                return None
            return OtnLineServiceIntentUnderlayTech.create_new(
                selected_tech=orig_utech.selected_tech,
                otn=copy_otn_tech(orig_otn=orig_utech.otn_non_throwing),
                wdm=copy_wdm_tech(orig_wdm=orig_utech.wdm_non_throwing)
            )

        def copy_elan_underlay_tech(orig_utech: Optional[MplsUnderlayTech])->Optional[MplsUnderlayTech]:
            if not orig_utech:
                return None
            return MplsUnderlayTech.create_new(
                selected_tech=orig_utech.selected_tech,
                ip_mpls=copy_ip_mpls_tech(orig_ip_mpls=orig_utech.ip_mpls_non_throwing),
                mpls_tp=copy_mpls_tp_tech(orig_mpls_tp=orig_utech.mpls_tp_non_throwing)
            )

        def copy_eline_underlay_tech(orig_utech: Optional[ELineServiceIntentUnderlayTech])->Optional[ELineServiceIntentUnderlayTech]:
            if not orig_utech:
                return None
            return ELineServiceIntentUnderlayTech.create_new(
                selected_tech=orig_utech.selected_tech,
                ip_mpls=copy_ip_mpls_tech(orig_ip_mpls=orig_utech.ip_mpls_non_throwing),
                mpls_tp=copy_mpls_tp_tech(orig_mpls_tp=orig_utech.mpls_tp_non_throwing),
                otn=copy_otn_tech(orig_otn=orig_utech.otn_non_throwing),
                wdm=copy_wdm_tech(orig_wdm=orig_utech.wdm_non_throwing),
            )

        def copy_pcs(orig_pcs: Optional[PathComputationSettings])->Optional[PathComputationSettings]:
            if not orig_pcs:
                return None
            return PathComputationSettings.create_new(
                backup_path_optimization = orig_pcs.backup_path_optimization,
                include_link_states = frozenset(x for x in orig_pcs.include_link_states) if orig_pcs.include_link_states else frozenset(),
                main_path_optimization=orig_pcs.main_path_optimization
            )
        def copy_protection_settings(orig_protection_settings: Optional[ProtectionSettings])->Optional[ProtectionSettings]:
            def copy_resources_diversity(orig_rd: Optional[ResourceDiversity])->Optional[ResourceDiversity]:
                if not orig_rd:
                    return None
                return ResourceDiversity.create_new(
                    diversity_policy=orig_rd.diversity_policy,
                    diversed_resources=frozenset(x for x in orig_rd.diversed_resources) if orig_rd.diversed_resources else frozenset()
                )
            if not orig_protection_settings:
                return None
            return ProtectionSettings.create_new(
                protection_policy=orig_protection_settings.protection_policy,
                resource_diversity_for_1_plus_1=copy_resources_diversity(orig_rd=orig_protection_settings.resource_diversity_for_1_plus_1_throwing)
            )

        def copy_data_for_non_multiplexed_service(orig_raw_eth_service_data: ELineServiceIntentRawEthServiceData)->Optional[ELineServiceIntentRawEthServiceData]:
            if not orig_raw_eth_service_data:
                return None
            return ELineServiceIntentRawEthServiceData.create_new(
                eth_port_type= orig_raw_eth_service_data.eth_port_type
            )

        def copy_cos_settings(orig_qos: Optional[QosSettings])->Optional[QosSettings]:
            if not orig_qos:
                return None
            return QosSettings.create_new(
                cbs_kb=orig_qos.cbs_kb,
                ebs_kb=orig_qos.ebs_kb,
                eir=orig_qos.eir,
                cir=orig_qos.cir,
                qos_policy=orig_qos.qos_policy
            )

        def copy_si_resources(orig_si_resources: Optional[FrozenSet[ServiceIntentResource]])->Optional[FrozenSet[ServiceIntentResource]]:
            if not  orig_si_resources:
                return None
            return orig_si_resources

        def copy_si_related_services(orig_si_related_services: Optional[FrozenSet[Service]])->Optional[FrozenSet[Service]]:
            if not orig_si_related_services:
                return None
            return orig_si_related_services

        path_computation_settings: PathComputationSettings = copy_pcs(orig_pcs=orig.path_computation_settings_non_throwing)

        orig_si_type: ServiceIntentTypeEnum = orig.type
        si_resources = copy_si_resources(orig_si_resources= orig.resources)
        si_related_services = copy_si_related_services(orig_si_related_services = orig.related_services)
        si = None
        if orig_si_type == ServiceIntentTypeEnum.E_LINE:
            orig: ELineServiceIntent = orig
            protection_settings: ProtectionSettings = copy_protection_settings(orig_protection_settings=orig.protection_settings_non_throwing)
            si = ELineServiceIntent.create_new(
                base_template=orig.base_template,
                customer_name=orig.customer_name,
                desc=orig.desc,
                guid=orig.guid,
                deployment_info=orig.deployment_info_non_throwing,
                name=orig.name,
                is_oam_pm_collection_enabled=orig.is_oam_pm_collection_enabled,
                provider = orig.provider,
                path_computation_settings=path_computation_settings,
                protection_settings=protection_settings,
                underlay_tech=copy_eline_underlay_tech(orig_utech=orig.underlay_tech_non_throwing),
                vlan_manipulation=orig.vlan_manipulation,
                data_for_non_multiplexed_service=copy_data_for_non_multiplexed_service(orig_raw_eth_service_data = orig.data_for_non_multiplexed_service_non_throwing),
                qos_settings=copy_cos_settings(orig_qos = orig.qos_settings_non_throwing),
                is_template=orig.is_template,
                extra=orig.extra.copy()
            )
        elif orig_si_type == ServiceIntentTypeEnum.OTN_LINE:
            orig: OtnLineServiceIntent = orig
            protection_settings: ProtectionSettings = copy_protection_settings(
                orig_protection_settings=orig.protection_settings_non_throwing)
            si= OtnLineServiceIntent.create_new(
                base_template=orig.base_template,
                customer_name=orig.customer_name,
                desc=orig.desc,
                guid=orig.guid,
                deployment_info=orig.deployment_info_non_throwing,
                name=orig.name,
                provider=orig.provider,
                path_computation_settings=path_computation_settings,
                protection_settings=protection_settings,
                underlay_tech=copy_otn_line_underlay_tech(orig_utech=orig.underlay_tech_non_throwing),
                is_template=orig.is_template,
                extra=orig.extra.copy(),
                odu_type=orig.odu_type
            )
        elif orig_si_type == ServiceIntentTypeEnum.L3_VPN:
            orig: L3VpnServiceIntent = orig
            protection_settings: ProtectionSettings = copy_protection_settings(
                orig_protection_settings=orig.protection_settings_non_throwing)
            si = L3VpnServiceIntent.create_new(
                base_template=orig.base_template,
                customer_name=orig.customer_name,
                desc=orig.desc,
                guid=orig.guid,
                deployment_info=orig.deployment_info_non_throwing,
                name=orig.name,
                is_oam_pm_collection_enabled=orig.is_oam_pm_collection_enabled,
                provider=orig.provider,
                path_computation_settings=path_computation_settings,
                protection_settings=protection_settings,
                underlay_tech=copy_l3vpn_underlay_tech(orig_utech=orig.underlay_tech_non_throwing),
                resource_allocation_policy=orig.resource_allocation_policy,
                route_distinguisher=orig.route_distinguisher,
                vpn_topology=orig.vpn_topology,
                any_to_any_details=copy_l3vpn_any_to_any(orig_details=orig.any_to_any_details_non_throwing),
                hub_and_spoke_details=copy_l3vpn_hub_and_spoke(orig_details=orig.hub_and_spoke_details_non_throwing),
                qos_settings=copy_cos_settings(orig_qos=orig.qos_settings_non_throwing),
                is_template=orig.is_template,
                extra=orig.extra.copy()
            )
        elif orig_si_type == ServiceIntentTypeEnum.E_LAN:
            orig: ELanServiceIntent = orig
            protection_settings: ProtectionSettings = copy_protection_settings(
                orig_protection_settings=orig.protection_settings_non_throwing)
            si = ELanServiceIntent.create_new(
                base_template=orig.base_template,
                customer_name=orig.customer_name,
                desc=orig.desc,
                guid=orig.guid,
                deployment_info=orig.deployment_info_non_throwing,
                name=orig.name,
                provider=orig.provider,
                path_computation_settings=path_computation_settings,
                protection_settings=protection_settings,
                underlay_tech=copy_elan_underlay_tech(orig_utech=orig.underlay_tech_non_throwing),
                vlan_manipulation=orig.vlan_manipulation,
                is_template=orig.is_template,
                extra=orig.extra.copy()
            )
        elif orig_si_type == ServiceIntentTypeEnum.E_TREE:
            orig: ETreeServiceIntent = orig
            protection_settings: ProtectionSettings = copy_protection_settings(
                orig_protection_settings=orig.protection_settings_non_throwing)
            si = ETreeServiceIntent.create_new(
                base_template=orig.base_template,
                customer_name=orig.customer_name,
                desc=orig.desc,
                guid=orig.guid,
                deployment_info=orig.deployment_info_non_throwing,
                name=orig.name,
                provider=orig.provider,
                path_computation_settings=path_computation_settings,
                protection_settings=protection_settings,
                underlay_tech=copy_elan_underlay_tech(orig_utech=orig.underlay_tech_non_throwing),
                vlan_manipulation=orig.vlan_manipulation,
                is_template=orig.is_template,
                extra=orig.extra.copy()
            )
        elif orig_si_type == ServiceIntentTypeEnum.SDH_LINE:
            orig: SdhLineServiceIntent = orig
            protection_settings: ProtectionSettings = copy_protection_settings(
                orig_protection_settings=orig.protection_settings_non_throwing)
            si = SdhLineServiceIntent.create_new(
                base_template=orig.base_template,
                customer_name=orig.customer_name,
                desc=orig.desc,
                guid=orig.guid,
                deployment_info=orig.deployment_info_non_throwing,
                name=orig.name,
                provider=orig.provider,
                path_computation_settings=path_computation_settings,
                protection_settings=protection_settings,
                is_template=orig.is_template,
                extra=orig.extra.copy()
            )

        si.set_resources(value=si_resources)
        si.set_related_services(value=si_related_services)
        return si


    def _copy_service(self, orig: PceService) -> PceService:
        return PceService(
            endpoints=[
                self._copy_endpoint(orig=orig.endpoints[0]),
                self._copy_endpoint(orig=orig.endpoints[1])
            ],
            service_name=orig.service_name,
            service_type=orig.service_type,
            settings=Settings(
                general_settings=self._copy_general_settings(orig=orig.settings.general_settings),
                path_computation_settings=self._copy_path_computation_setiings(
                    orig=orig.settings.path_computation_settings),
                primary_path_settings=self._copy_path_settings(orig=orig.settings.primary_path_settings),
                secondary_path_settings=self._copy_path_settings(orig=orig.settings.secondary_path_settings)
            )
        )

    @staticmethod
    def build_eline_si(model:SimpleModelPojoPool, params: Dict[Union[Enum, str], Any])->ELineServiceIntent:
        path_computation_settings = PathComputationSettings.create_new(
            backup_path_optimization=PathOptimizationEnum[params[ServiceRequestConstants.SECONDARY_PATH][ServiceRequestConstants.PATH_OPTIMIZATION_TYPE]],
            main_path_optimization=PathOptimizationEnum[params[ServiceRequestConstants.SECONDARY_PATH][ServiceRequestConstants.PATH_OPTIMIZATION_TYPE]],
            include_link_states=frozenset({LinkStateEnum.OPERATIONALLY_UP, LinkStateEnum.OPERATIONALLY_DOWN, LinkStateEnum.IN_MAINTENANCE})
        )
        protection_settings = ProtectionSettings.create_new(
            protection_policy=ProtectionPolicyEnum[params.get(ServiceRequestConstants.PROTECTION_POLICY, ProtectionPolicyEnum.UNPROTECTED.name)],
            resource_diversity_for_1_plus_1 = ResourceDiversity.create_new(
                diversed_resources=frozenset(params.get(ServiceRequestConstants.DIVERSITY_RESOURCES, {DiversedResourcesEnum.SRLG, DiversedResourcesEnum.LINK})),
                diversity_policy=DiversityPolicyEnum.STRICT
            )
        )
        underlay_tech = ELineServiceIntentUnderlayTech.create_new(
            selected_tech=params.get(ServiceRequestConstants.ALLOWED_UNDERLAY_OPTIONS, ELineServiceIntentSupportedUnderlayTechsEnum.OTN),
            ip_mpls=params.get(ELineServiceIntentSupportedUnderlayTechsEnum.IP_MPLS, None),
            mpls_tp=params.get(ELineServiceIntentSupportedUnderlayTechsEnum.MPLS_TP, None),
            otn = params.get(ELineServiceIntentSupportedUnderlayTechsEnum.OTN, None),
            wdm=params.get(ELineServiceIntentSupportedUnderlayTechsEnum.WDM, None)
        )
        guid = "SI/"+params.get(ServiceRequestConstants.SERVICE_NAME, "").replace(" ", "-")
        LOG.info(guid)

        si = ELineServiceIntent.create_new(
            base_template=None,
            customer_name="PCE Tester",
            deployment_info=DeploymentInfo.create_new(
                last_status_change_time_stamp=None,
                operation=DeploymentOperationEnum.PLANNED,
                phase=DeploymentOperationPhaseEnum.IN_PROGRESS
            ),
            desc=params.get("description", ""),
            guid=guid,
            name=params.get(ServiceRequestConstants.SERVICE_NAME, ""),
            is_oam_pm_collection_enabled = False,
            path_computation_settings=path_computation_settings,
            protection_settings=protection_settings,
            provider="PCE Tester",
            underlay_tech = underlay_tech,
            vlan_manipulation = VlanManipulationEnum.NO_MANIPULATION,
            data_for_non_multiplexed_service=ELineServiceIntentRawEthServiceData.create_new(eth_port_type=EthPortTypeEnum.ETH_10G),
            qos_settings=QosSettings.create_new(
                cir=params.get(ServiceRequestConstants.CIR, None),
                eir=params.get(ServiceRequestConstants.EIR, None),
                cbs_kb=params.get(ServiceRequestConstants.CBS, None),
                ebs_kb=params.get(ServiceRequestConstants.EBS, None),
                qos_policy=None
            ),
            is_template=False,
            extra=dict()
        )
        resources: Set[ServiceIntentResource] = set()
        for num, value in enumerate(params[ServiceRequestConstants.ENDPOINTS]):
            resources.add(
                EndpointServiceIntentResource.create_new(
                    order=num,
                    guid="IR/"+value[ServiceRequestConstants.GUID],
                    resource=model.get_by_guid(guid=value[ServiceRequestConstants.GUID]),
                    layer_1_info=value.get("l1", None),
                    layer_2_info=value.get("l2", None),
                    layer_3_info=value.get("l3", None),
                    service_intent=si.as_ref(),
                    extra=dict()
                )
            )
        if params[ServiceRequestConstants.SECONDARY_PATH][ServiceRequestConstants.INCLUDE_RESOURCES]:
            for idx, include in enumerate(params[ServiceRequestConstants.SECONDARY_PATH][ServiceRequestConstants.INCLUDE_RESOURCES]):
                resources.add(
                    IncludeServiceIntentResource.create_new(
                        order=idx,
                        guid="IR/"+include,
                        protection_role=ProtectionRoleEnum.PROTECTION_SECONDARY,
                        resource=model.get_by_guid(guid=include),
                        service_intent=si.as_ref(),
                        extra=dict()
                    )
                )
        if params[ServiceRequestConstants.SECONDARY_PATH][ServiceRequestConstants.EXCLUDE_RESOURCES]:
            for idx, exclude in enumerate(params[ServiceRequestConstants.SECONDARY_PATH][ServiceRequestConstants.EXCLUDE_RESOURCES]):
                resources.add(
                    ExcludeServiceIntentResource.create_new(
                        order=idx,
                        guid="IR/"+exclude,
                        protection_role=ProtectionRoleEnum.PROTECTION_SECONDARY,
                        resource=model.get_by_guid(guid=exclude),
                        service_intent=si.as_ref(),
                        extra=dict()
                    )
                )
        if params[ServiceRequestConstants.PRIMARY_PATH][ServiceRequestConstants.INCLUDE_RESOURCES]:
            for idx, include in enumerate(params[ServiceRequestConstants.PRIMARY_PATH][ServiceRequestConstants.INCLUDE_RESOURCES]):
                resources.add(
                    IncludeServiceIntentResource.create_new(
                        order=idx,
                        protection_role=ProtectionRoleEnum.PROTECTION_PRIMARY,
                        guid="IR/"+include,
                        resource=model.get_by_guid(guid=include),
                        service_intent=si.as_ref(),
                        extra=dict()
                    )
                )
        if params[ServiceRequestConstants.PRIMARY_PATH][ServiceRequestConstants.EXCLUDE_RESOURCES]:
            for idx, exclude in enumerate(params[ServiceRequestConstants.PRIMARY_PATH][ServiceRequestConstants.EXCLUDE_RESOURCES]):
                resources.add(
                    ExcludeServiceIntentResource.create_new(
                        order=idx,
                        guid="IR/" + exclude,
                        protection_role=ProtectionRoleEnum.PROTECTION_PRIMARY,
                        resource=model.get_by_guid(guid=exclude),
                        service_intent=si.as_ref(),
                        extra=dict()
                    )
                )


        si.set_resources(value=resources)
        return si

    @staticmethod
    def build_elan_si(model: SimpleModelPojoPool, **kwargs) -> ELanServiceIntent:
        return ELanServiceIntent.create_new(

        )

    @staticmethod
    def build_etree_si(model: SimpleModelPojoPool, **kwargs) -> ETreeServiceIntent:
        return ETreeServiceIntent.create_new(

        )

    @staticmethod
    def build_otn_line_si(model: SimpleModelPojoPool, **kwargs) -> OtnLineServiceIntent:
        return OtnLineServiceIntent.create_new(

        )

    @staticmethod
    def build_sdh_line_si(model: SimpleModelPojoPool, **kwargs) -> SdhLineServiceIntent:
        return SdhLineServiceIntent.create_new(

        )

    @staticmethod
    def build_l3vpn_si(model: SimpleModelPojoPool, **kwargs) -> L3VpnServiceIntent:
        return L3VpnServiceIntent.create_new(

        )

    async def _provide(self, si: bool,params: Dict[Union[Enum, str], Any]):
        await self._model_helper.run(self.app)
        model = self._model_helper.pool
        service_type=params.get(ServiceRequestConstants.SERVICE_TYPE)

        if si:
            new_si = partial({
                ServiceIntentTypeEnum.E_LINE: PathCalculatorMock.build_eline_si,
                ServiceIntentTypeEnum.E_LAN: PathCalculatorMock.build_elan_si,
                ServiceIntentTypeEnum.E_TREE: PathCalculatorMock.build_etree_si,
                ServiceIntentTypeEnum.OTN_LINE: PathCalculatorMock.build_otn_line_si,
                ServiceIntentTypeEnum.SDH_LINE: PathCalculatorMock.build_sdh_line_si,
                ServiceIntentTypeEnum.L3_VPN: PathCalculatorMock.build_l3vpn_si
            }.get(service_type))(
                model=model,
                params=params
            )
            pool, proposed_paths = await self._calculate_paths_si(curr=new_si, prev=None)
            await self.visualize_proposed_paths_si(si=new_si, model=pool, calculated_paths=proposed_paths)
        else:
            new_config = PceService(
                endpoints=[
                    Endpoint(
                        # port='PO/eth/44c2c933297e5bda/861426eb6d3bfe5e',
                        # port='PO/optical_client/HUAWEI-ACTN/0.190.0.3/67043332',
                        port='PO/optical_client/HUAWEI-ACTN/0.190.0.3/50266121',
                        vlan_id=1001,
                        cir=1000,
                        capability='Eth10GigLAN',
                        eir=None,
                        cbs=None,
                        ebs=None
                    ),
                    Endpoint(
                        # port='PO/eth/5b86b7d24964e426/861426eb6d3bfe5e',
                        # port='PO/optical_client/HUAWEI-ACTN/0.191.0.4/67043332',
                        port='PO/optical_client/HUAWEI-ACTN/0.191.0.4/50266121',

                        vlan_id=1001,
                        cir=1000,
                        capability='Eth10GigLAN',
                        eir=None,
                        cbs=None,
                        ebs=None
                    ),
                ],
                service_name="Test 2.3.6 EPL including NE and Link",
                service_type=ServiceType.E_LINE,
                settings=Settings(
                    general_settings=GeneralSettings(
                        allowed_underlay_options=ELineServiceIntentSupportedUnderlayTechsEnum.OTN,
                        service_tunnel_type=UnderlayTechOtnServiceTunnelTypeEnum.ODU_K,
                        service_tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.ODU2E,
                        protection_policy=ProtectionPolicyEnum.UNPROTECTED,
                        allowed_nni_options=[],  # [x for x in UnderlayTechNNITechEnum],
                        tunnel_policy=TunnelPolicy(
                            technology_type=UnderlayTechIpMplsTunnelTypeEnum.LDP,
                            tunnel_usage_constraints=UnderlayTechMplsTpTunnelUsageConstraintsEnum.CREATE_IF_NOT_EXIST,
                            tunnel_resiliency=UnderlayTechMplsTunnelResilliencyEnum.NO_RESILLIENCY
                        )
                    ),
                    path_computation_settings=PcePathComputationSettings(
                        included_link_states=[
                            LinkStateEnum.OPERATIONALLY_UP
                        ],
                        diversity_resources=[
                            DiversedResourcesEnum.LINK,
                            DiversedResourcesEnum.SRLG
                        ],
                        diversity_policy=DiversityPolicyEnum.STRICT
                    ),
                    primary_path_settings=PathSettings(
                        path_optimization_type=PathOptimizationEnum.ADMIN_COST,
                        include_resources=[
                            'IN/HUAWEI-ACTN/0.190.0.2',
                            'LI/persist/stitched-idl-PO/otu/HUAWEI-ACTN/0.190.0.2/107~PO/otu/HUAWEI-ACTN/0.191.0.2/119',
                            'LI/persist/stitched-idl-PO/otu/HUAWEI-ACTN/0.190.0.1/121~PO/otu/HUAWEI-ACTN/0.191.0.1/175',
                            # 'IN/HUAWEI-ACTN/0.190.0.1',
                            'IN/HUAWEI-ACTN/0.191.0.3',
                            # 'LI/persist/stitched-idl-PO/otu/HUAWEI-ACTN/0.190.0.1/121~PO/otu/HUAWEI-ACTN/0.191.0.1/175',
                            # 'IN/HUAWEI-ACTN/0.190.0.1',
                            # 'IN/HUAWEI-ACTN/0.191.0.4'

                            # 'IN/HUAWEI-ACTN/0.191.0.2'
                            # 'LI/oms/HUAWEI-ACTN/0.190.0.4/7:0.190.0.6/7',
                            # 'IN/HUAWEI-ACTN/0.191.0.4'
                            # 'LI/otu/HUAWEI-ACTN/0.191.0.1/6:0.191.0.3/18',
                            # 'IN/HUAWEI-ACTN/0.190.0.2',
                            # 'LI/persist/stitched-idl-PO/otu/HUAWEI-ACTN/0.190.0.2/107~PO/otu/HUAWEI-ACTN/0.191.0.2/119',
                            # 'IN/HUAWEI-ACTN/0.191.0.1'
                            # 'LI/otu/HUAWEI-ACTN/0.191.0.1/6:0.191.0.3/18'
                        ],
                        exclude_resources=[

                        ]
                    ),
                    secondary_path_settings=PathSettings(
                        path_optimization_type=PathOptimizationEnum.NUMBER_OF_HOPS,
                        include_resources=[

                        ],
                        exclude_resources=[

                        ]
                    )
                )
            )

            pool, proposed_paths = await self._calculate_proposed_paths(new_config=new_config,
                                                                        previous_config={'selected_guid': ''},
                                                                        guid_service_to_modify=None)
            await self.visualize_proposed_paths(config=new_config, model=pool, calculated_paths=proposed_paths)

    async def execute(self):
        await self._provide(si=True, params=ELINE_OTN_BASE)
        # await self._modify(si=True)

    async def _calculate_paths_si(self, curr: ServiceIntent, prev: Optional[ServiceIntent] = None)-> Tuple[
        SimpleModelPojoPool, CalculatedPaths]:

        pool, cache, service_request = await build_service_request(
            app=self.app,
            service_params=curr,
            previous_service_params=prev
        )
        calculated_paths: CalculatedPaths = await calculate_service_path(app=self.app, pool=pool, cache=cache,
                                                                         service_request=service_request)
        return pool, calculated_paths

    async def _calculate_proposed_paths(self, new_config: PceService,
                                        previous_config: Union[PceService, Dict[str, str], None] = None,
                                        guid_service_to_modify: Optional[str] = None) -> Tuple[
        SimpleModelPojoPool, CalculatedPaths]:
        # try:
        pool, cache, service_request = await build_service_request(
            app=self.app,
            service_params=new_config,
            previous_service_params=previous_config,
            service_guid=guid_service_to_modify,
            with_service_intent=True
        )
        calculated_paths: CalculatedPaths = await calculate_service_path(app=self.app, pool=pool, cache=cache,
                                                                         service_request=service_request)
        return pool, calculated_paths

    # except ValueError as ve:
    #     self._msg_box.set_message(str(ve))
    # except CustomError as ce:
    #     self._msg_box.set_message(ce._get_error_and_details_txt())

    def _build_service_object(self, r_params: Dict[str, Any]) -> PceService:
        src = Endpoint(
            port=r_params[ServiceRequestConstants.ENDPOINTS][0][ServiceRequestConstants.PORT],
            vlan_id=r_params[ServiceRequestConstants.ENDPOINTS][0].get(ServiceRequestConstants.VLAN_ID,
                                                                       None),
            cir=r_params[ServiceRequestConstants.ENDPOINTS][0].get(ServiceRequestConstants.CIR, None),
            capability=r_params[ServiceRequestConstants.ENDPOINTS][0].get(
                ServiceRequestConstants.CAPABILITY, None),
            eir=r_params[ServiceRequestConstants.ENDPOINTS][0].get(ServiceRequestConstants.EIR, None),
            cbs=r_params[ServiceRequestConstants.ENDPOINTS][0].get(ServiceRequestConstants.CBS, None),
            ebs=r_params[ServiceRequestConstants.ENDPOINTS][0].get(ServiceRequestConstants.EBS, None)
        )
        dst = Endpoint(
            port=r_params[ServiceRequestConstants.ENDPOINTS][1][ServiceRequestConstants.PORT],
            vlan_id=r_params[ServiceRequestConstants.ENDPOINTS][1].get(ServiceRequestConstants.VLAN_ID,
                                                                       None),
            cir=r_params[ServiceRequestConstants.ENDPOINTS][1].get(ServiceRequestConstants.CIR, None),
            capability=r_params[ServiceRequestConstants.ENDPOINTS][1].get(
                ServiceRequestConstants.CAPABILITY, None),
            eir=r_params[ServiceRequestConstants.ENDPOINTS][1].get(ServiceRequestConstants.EIR, None),
            cbs=r_params[ServiceRequestConstants.ENDPOINTS][1].get(ServiceRequestConstants.CBS, None),
            ebs=r_params[ServiceRequestConstants.ENDPOINTS][1].get(ServiceRequestConstants.EBS, None)
        )
        endpoints = [
            src,
            dst
        ]
        settings = Settings(
            general_settings=GeneralSettings(
                allowed_underlay_options=ELineServiceIntentSupportedUnderlayTechsEnum.OTN,
                service_tunnel_type=UnderlayTechOtnServiceTunnelTypeEnum.ODU_FLEX,
                service_tunnel_rate=UnderlayTechOtnServiceTunnelRateEnum.AUTO_SIZE,
                protection_policy=ProtectionPolicyEnum.INTRA_AREA_1_PLUS_1_PROTECTED,
                allowed_nni_options=[
                    UnderlayTechNNITechEnum.OTU2,
                    UnderlayTechNNITechEnum.OTU2E,
                    UnderlayTechNNITechEnum.OTU3,
                    UnderlayTechNNITechEnum.OTU3E2,
                    UnderlayTechNNITechEnum.OTU4
                ],
                tunnel_policy=TunnelPolicy(
                    technology_type=UnderlayTechIpMplsTunnelTypeEnum.LDP,
                    tunnel_usage_constraints=UnderlayTechMplsTpTunnelUsageConstraintsEnum.CREATE_IF_NOT_EXIST,
                    tunnel_resiliency=UnderlayTechMplsTunnelResilliencyEnum.NO_RESILLIENCY
                )
            ),
            path_computation_settings=PcePathComputationSettings(
                included_link_states=[
                    LinkStateEnum.OPERATIONALLY_UP,
                    LinkStateEnum.OPERATIONALLY_DOWN,
                    LinkStateEnum.IN_MAINTENANCE
                ],
                diversity_resources=[
                    DiversedResourcesEnum.LINK,
                    DiversedResourcesEnum.SRLG
                ],
                diversity_policy=DiversityPolicyEnum.BEST_EFFORT
            ),
            primary_path_settings=PathSettings(
                path_optimization_type=PathOptimizationEnum.NUMBER_OF_HOPS,
                include_resources=[

                ],
                exclude_resources=[
                    'LI/oms/HUAWEI-ACTN/0.191.0.5/9:0.191.0.8/13',
                    'IN/HUAWEI-ACTN/0.190.0.2'
                ]
            ),
            secondary_path_settings=PathSettings(
                path_optimization_type=PathOptimizationEnum.NUMBER_OF_HOPS,
                include_resources=[

                ],
                exclude_resources=[
                    'LI/oms/HUAWEI-ACTN/0.191.0.5/9:0.191.0.8/13',
                    'IN/HUAWEI-ACTN/0.190.0.2'
                ]
            )
        )
        return PceService(
            service_type=ServiceType.E_LINE,
            endpoints=endpoints,
            settings=settings,
            service_name="Test Service New"
        )


if __name__ == '__main__':
    PathCalculatorMock.add_global_style_file(path.join(path.dirname(__file__), 'path_calculation_engine.css'))

    App.start_and_run(
        WebServer(
            base_app=PathCalculatorMock.create_web_app(),
            extra_apps=[]
        )
    )
