from nfdk.pyoneer.app import App
from nfdk.pyoneer.web_server import WebServer
from nfdk.pyoneer.env import DYNAMIC_APP_METADATA
from nfdk.pyoneer.appui import PyoneerAppUI
from nfdk.pyoneer.components import SinglePageTemplate


class PceTest(PyoneerAppUI):
    def __init__(self):
        self.set_root(SinglePageTemplate(
            title=DYNAMIC_APP_METADATA['name'],
            children=[])
        )


if __name__ == '__main__':
    App.start_and_run(WebServer(base_app=PceTest.create_web_app(), extra_apps=[]))
