#!/usr/bin/env bash

set -ex

#
# Placeholder for app specific extra build-script hook.
# Script file must be named `build.sh` and must be executable.
# Invoked before docker image is built.
#

